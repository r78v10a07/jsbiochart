# JSBioChart

## Instalation

```bash
    npm install
    webpack
```

The js file is: ```dist/jsbiochart.js```

## Dependency

|[D3](https://d3js.org/) (required)|
| --- |
| 4+ |

Load billboard.js after D3.js.

```html
<!-- 1) Load D3.js and jsbiochart.js separately -->
    <!-- Load D3 -->
    <script src="https://d3js.org/d3.v4.min.js"></script>
    
    <!-- Load jsbiochart.js with base(or theme) style -->
    <link rel="stylesheet" href="$YOUR_PATH/jsbiochart.css">
    <script src="$YOUR_PATH/jsbiochart.js"></script>
    
```

## Basic usage example

#### 1) Create chart holder element
```html
<div id="chart"></div>
```

#### 2) Generate a chart with options
```js
// generate the chart
const chart = jsbiochart.generate({
    id: id,
    data: {
        elements: elements,
        data: data
    },
    domain: [-1, 1],
    color: {
        domain: [-1, 0, 1],
        range: ['#5e011d', 'white', '#092a56']
    },
    boxsize: function (elements) {
        return (elements.length <= 20) ? 25 : (elements.length > 20 && elements.length <= 60) ? 17 : 13;
    },
    tip: d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function (d) {
            return "<div class='mygraph-font'>" + d.tip + "</div>";
        }),
    onclick: heatmap_onclick,
    chart_type: "heatmap"
});
chart.draw();
```

#### Example

<img src="https://gitlab.com/r78v10a07/jsbiochart/raw/master/img/example.png" alt="Correlation Plot"  width="888" />

# Public Domain notice

National Center for Biotechnology Information.

This software is a "United States Government Work" under the terms of the United States
Copyright Act. It was written as part of the authors' official duties as United States
Government employees and thus cannot be copyrighted. This software is freely available
to the public for use. The National Library of Medicine and the U.S. Government have not
 placed any restriction on its use or reproduction.

Although all reasonable efforts have been taken to ensure the accuracy and reliability
of the software and data, the NLM and the U.S. Government do not and cannot warrant the
performance or results that may be obtained by using this software or data. The NLM and
the U.S. Government disclaim all warranties, express or implied, including warranties
of performance, merchantability or fitness for any particular purpose.

Please cite NCBI in any work or product based on this material.

