import Chart from "./internals/Chart";

/**
 * @namespace jsbiochart
 * @version #__VERSION__#
 */
const jsbiochart = {
    /**
     * Version information
     * @property {String} version version
     * @example
     *    jsbiochart.version;  // "1.0.0"
     * @memberOf jsbiochart
     */
    version: "#__VERSION__#",

    generate(config) {
        const inst = new Chart(config);
        this.instance.push(inst);
        return inst;
    },

    instance: [],

    chart: {
        fn: Chart.prototype,
    }

};

export {jsbiochart};
export default jsbiochart;