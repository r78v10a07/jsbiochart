export default class BaseChart {
    constructor(config) {
        const $$ = this;
        $$.config = config;

        $$.onclick = ('onclick' in $$.config) ? $$.config.onclick : undefined;
        $$.tip = ('tip' in $$.config) ? $$.config.tip : $$.default_tip();
        $$.domain = ('domain' in $$.config) ? $$.config.domain : alert('domain is undefined in the config');
    }

    default_tip() {
        const tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function (d) {
                return "<div class='mygraph-font'>" + JSON.stringify(d) + "</div>";
            });
        return tip;
    }
};
