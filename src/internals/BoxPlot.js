import BaseChart from "./BaseChart";

export default class BoxPLot {
    constructor(config) {
        const $$ = this;
        this.base = new BaseChart(config);
    }

    draw() {
        const $$ = this;
        console.log('Drawing BoxPlot');
        console.log($$.base.config);
    }
};
