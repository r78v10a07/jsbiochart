import BoxPlot from "./BoxPlot";
import HeatMap from "./HeatMap";

export default class Chart {
    constructor(config) {
        this.config = config;

        // bind "this" to nested API
        (function bindThis(fn, target, argThis) {
            Object.keys(fn).forEach(key => {
                target[key] = fn[key].bind(argThis);

                Object.keys(fn[key]).length &&
                bindThis(fn[key], target[key], argThis);
            });
        })(Chart.prototype, this, this);
    }

    draw() {
        if (this.config.chart_type == "boxplot") {
            const chart = new BoxPlot(this.config);
            chart.draw();
        } else if (this.config.chart_type == "heatmap") {
            const chart = new HeatMap(this.config);
            chart.draw();
        }

    }
}