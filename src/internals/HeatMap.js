import BaseChart from "./BaseChart";

export default class HeatMap {
    constructor(config) {
        const $$ = this;
        this.base = new BaseChart(config);
    }

    boxsize(elements) {
        return (elements.length <= 20) ? 25 : (elements.length > 20 && elements.length <= 60) ? 17 : 13;
    }

    draw() {
        const $$ = this;
        console.log('Drawing HeatMap');
        console.log($$.base.config);

        const id = $$.base.config.id;
        const elements = $$.base.config.data.elements;
        const data = $$.base.config.data.data;

        const boxsize = ('boxsize' in $$.base.config) ? $$.base.config.boxsize(elements) : $$.boxsize(elements);

        const colorDomain = (('color' in $$.base.config) && ('domain' in $$.base.config.color)) ? $$.base.config.color.domain : [-1, 0, 1];
        const colorRange = (('color' in $$.base.config) && ('range' in $$.base.config.color)) ? $$.base.config.color.range : ['#5e011d', 'white', '#092a56'];

        const margin = {top: 60, right: 5, bottom: 10, left: 60};
        const cellSize = boxsize - 1;
        const legend_width = 100, legend_height = 10;
        const legend_data = d3.range($$.base.domain[0], $$.base.domain[1], 0.01);

        const width = (elements.length * cellSize + 20 > legend_width) ? elements.length * cellSize + 20 : legend_width;
        const height = elements.length * cellSize + 20;

        if (width < legend_width) {
            margin.right = 8;
        }

        const x_legend_scale = d3.scaleLinear().range([0, legend_width]);
        const x_legend_axis = d3.axisBottom().scale(x_legend_scale).ticks(3);
        x_legend_scale.domain($$.base.domain);

        const xScale = d3.scaleBand()
            .domain(elements)
            .range([0, elements.length * cellSize]);

        const xAxis = d3.axisTop(xScale)
            .tickSizeInner(0) // the inner ticks will be of size 3
            .tickSizeOuter(0);

        const yScale = d3.scaleBand()
            .domain(elements)
            .range([0, elements.length * cellSize]);

        const yAxis = d3.axisLeft(yScale)
            .tickSizeInner(0) // the inner ticks will be of size 3
            .tickSizeOuter(0);

        const colorScale = d3.scaleLinear()
            .domain(colorDomain)
            .range(colorRange);

        const svg = d3.select(id)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.call($$.base.tip);

        const cells = svg.selectAll('rect')
            .data(data)
            .enter().append('g').append('rect')
            .attr('class', 'cell')
            .attr('width', cellSize)
            .attr('height', cellSize)
            .attr('y', function (d) {
                return yScale(d.y);
            })
            .attr('x', function (d) {
                return xScale(d.x);
            })
            .attr('fill', function (d) {
                return colorScale(d.v);
            })
            .on('mouseover', function (d) {
                $$.base.tip.show(d);
            })
            .on('mouseout', function (d) {
                $$.base.tip.hide(d);
            })
            .on('click', function (d) {
                if (typeof $$.base.onclick !== 'undefined') {
                    $$.base.onclick(d);
                } else {
                    console.log("No onclick function defined");
                }
            });

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .selectAll('text')
            .attr('font-weight', 'normal');

        svg.append("g")
            .attr("class", "x axis")
            .call(xAxis)
            .selectAll('text')
            .attr('font-weight', 'normal')
            .style("text-anchor", "start")
            .attr("dx", ".4em")
            .attr("dy", ".5em")
            .attr("transform", function (d) {
                return "rotate(-65)";
            });

        const legend = svg.selectAll('.legend')
            .data(legend_data)
            .enter()
            .append("g")
            .append("rect")
            .attr("class", "bars")
            .attr("x", function (d) {
                return x_legend_scale(d);
            })
            .attr("y", -2)
            .attr("height", legend_height)
            .attr("width", 1)
            .style("fill", function (d) {
                return colorScale(d);
            })
            .attr("transform", "translate(0," + (height - (legend_height)) + ")");

        svg.append("g")
            .attr("class", "y axis")
            .call(x_legend_axis)
            .selectAll('text')
            .attr('font-weight', 'normal')
            .attr("transform", "translate(0," + (height - (legend_height)) + ")");
    }
};